# BLOG #
This was developed for Coursera Web Application Architectures course by University of New Mexico, which builds a blog app using Ruby on Rails.

### Features ###
1. There will be two types of users: the blog administrator and blog users.
2. The blog administrator should be able to enter new postings, typically
in reverse chronological order.
3. Users should be able to visit the blog site, and enter comments about
particular postings.
4. The blog administrator should be able to modify/remove any posting
or comment.
5. Users should not be able to modify postings or other users’ comments.